﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Timers;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Forms;
using Ookii.Dialogs.Wpf;

namespace EldenRingBackupManager
{
    public partial class BackupManagerForm : Form
    {

        private VistaFolderBrowserDialog BackupDirDialog;
        private VistaFolderBrowserDialog SaveDirDialog;
        private VistaFolderBrowserDialog AutoSaveDirDialog;
        private string BackupLocation;
        private string RestoreLocation;
        private string AutosaveLocation;
        private string Save_File_Name = "ER0000.sl2";
        private System.Timers.Timer autosave_timer;
        private string AUTOSAVE_DEFAULT_SELECT = "Please Select Autosave Interval Time (minutes)";
        private string auto_save_label_text = "autosave feature is currently: {0}";
        private string[] auto_save_status = { "on", "off" };
        private int autosave_interval;
        private int autosave_limit = 5;
        private int autosave_count = 0;
        private int autosave_naming_suffix = 0;
        private string autosave_location = Path.Combine(Directory.GetCurrentDirectory(), "autosave");

        private enum SAVE_STYLE
        {
            BKP,
            AUTO
        }

        public BackupManagerForm()
        {
            InitializeComponent();
        } 

        private void BackupBTN_Click(object sender, EventArgs e)
        {
            save_file(SAVE_STYLE.BKP);
        }

        private void save_file(SAVE_STYLE switcher)
        {
            string backupFileName = Save_File_Name;
            DateTime backupDate = DateTime.Now;
            string datestamp = backupDate.Day.ToString() + backupDate.Month.ToString() + backupDate.Year.ToString().Substring(2);
            datestamp += backupDate.Hour.ToString() + backupDate.Minute.ToString() + backupDate.Second.ToString();

            switch (switcher)
            {
                case SAVE_STYLE.BKP:
                    File.Copy(Path.Combine(RestoreLocation, Save_File_Name), Path.Combine(BackupLocation, Save_File_Name+"."+datestamp), true);
                    break;
                case SAVE_STYLE.AUTO:
                    File.Copy(Path.Combine(RestoreLocation, Save_File_Name), Path.Combine(autosave_location, Save_File_Name + ".autosave"+autosave_count), true);
                    break;
            }
        }

        private void RestoreBTN_Click(object sender, EventArgs e)
        {
            MessageBoxResult confirmResult = System.Windows.MessageBox.Show("You are about to overwrite the current save file.\nAre you sure?", "Restore Save File", MessageBoxButton.YesNo);

            if (confirmResult == System.Windows.MessageBoxResult.Yes)
            {
                DirectoryInfo backup_dir = new DirectoryInfo(BackupLocation);
                FileInfo backup_file = null;
                try
                {
                    backup_file = backup_dir.GetFiles().OrderByDescending(file => file.LastWriteTime).First();
                    File.Copy(Path.Combine(BackupLocation, backup_file.ToString()), Path.Combine(RestoreLocation, Save_File_Name), true);
                }
                catch (InvalidOperationException ex1)
                {
                    Console.WriteLine(ex1.Message);
                }
                catch (NullReferenceException ex2)
                {
                    Console.WriteLine(ex2.Message);
                }
            }

        }

        private void BackupManagerForm_Load(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(Environment.GetEnvironmentVariable("ER_SAVE_LOC", EnvironmentVariableTarget.User)))
            {
                
                string baseRestoreLocation = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "EldenRing");
                string steamIDFolder = "";
                string joinedRestoreLocation = "";
                Regex regexPattern = new Regex(@"\d{17}");

                foreach (string folder in Directory.EnumerateDirectories(baseRestoreLocation)){
                    MatchCollection matches = regexPattern.Matches(folder);
                    if (matches.Count >= 0)
                    {
                        steamIDFolder = matches[0].ToString();
                        break;
                    }
                };

                joinedRestoreLocation = Path.Combine(baseRestoreLocation, steamIDFolder);

                System.Environment.SetEnvironmentVariable("ER_SAVE_LOC", joinedRestoreLocation, EnvironmentVariableTarget.User);

            }
            else
            {
                RestoreLocation = Environment.GetEnvironmentVariable("ER_SAVE_LOC", EnvironmentVariableTarget.User);
            }

            if (String.IsNullOrEmpty(Environment.GetEnvironmentVariable("ER_BKP_LOC", EnvironmentVariableTarget.User)))
            {
                BackupLocation = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                System.Environment.SetEnvironmentVariable("ER_BKP_LOC", BackupLocation, EnvironmentVariableTarget.User);
            }
            else
            {
                BackupLocation = Environment.GetEnvironmentVariable("ER_BKP_LOC", EnvironmentVariableTarget.User);
            }

            if (String.IsNullOrEmpty(Environment.GetEnvironmentVariable("ER_AUTO_LOC", EnvironmentVariableTarget.User)))
            {
                string baseRestoreLocation = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "EldenRing");
                string autosave_folder = "autosave";
                string AutosavePath = Path.Combine(baseRestoreLocation, autosave_folder);
                AutosaveLocation = AutosavePath;

                Directory.CreateDirectory(AutosaveLocation);
                Environment.SetEnvironmentVariable("ER_AUTO_LOC", AutosavePath, EnvironmentVariableTarget.User);

            }
            else
            {
                AutosaveLocation = Environment.GetEnvironmentVariable("ER_AUTO_LOC", EnvironmentVariableTarget.User);
                
            }

            BackupLocationBox.Text = BackupLocation;
            SaveLocationBox.Text = RestoreLocation;
            AutosaveLocationBox.Text = AutosaveLocation;

            AutoSaveStatusLabel.Text = String.Format(auto_save_label_text, auto_save_status[1]);

            AutosaveIntervalSelector.Items.Insert(0, AUTOSAVE_DEFAULT_SELECT);
            AutosaveIntervalSelector.SelectedIndex = 0;

        }

        private void LocationSelectorBTN_Click(object sender, EventArgs e)
        {
            BackupDirDialog = new VistaFolderBrowserDialog();

            BackupDirDialog.ShowDialog();

           
            string[] backup_locations = BackupDirDialog.SelectedPaths;
            if (backup_locations != null && backup_locations.Length > 0)
            {
                string backup_location = backup_locations[0];
                BackupLocation = backup_location;
                BackupLocationBox.Text = backup_location;

                setNewBackupLocation(BackupLocation);
            }
        }

        private void SVELocationSelectorBTN_Click(object sender, EventArgs e)
        {
            SaveDirDialog = new VistaFolderBrowserDialog();
            
            SaveDirDialog.ShowDialog();

            string[] save_locations = SaveDirDialog.SelectedPaths;
            if (save_locations != null && save_locations.Length > 0)
            {
                string save_location = save_locations[0];
                RestoreLocation = save_location;
                SaveLocationBox.Text = save_location;

                setNewRestoreLocation(RestoreLocation);
            }
        }

        private void setNewBackupLocation(string newLoc)
        {
            System.Environment.SetEnvironmentVariable("ER_BKP_LOC", newLoc, EnvironmentVariableTarget.User);
        }

        private void setNewRestoreLocation(string newLoc)
        {
            System.Environment.SetEnvironmentVariable("ER_SAVE_LOC", newLoc, EnvironmentVariableTarget.User);
        }

        private void setNewAutosaveLocation(string newLoc)
        {
            System.Environment.SetEnvironmentVariable("ER_AUTO_LOC", newLoc, EnvironmentVariableTarget.User);
        }

        private void AutosaveIntervalSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!AutosaveIntervalSelector.SelectedItem.Equals(AUTOSAVE_DEFAULT_SELECT))
            {
                autosave_interval = Convert.ToInt32(AutosaveIntervalSelector.SelectedItem);
            }
            
        }

        private void AutosaveIntervalSelector_DropDown(object sender, EventArgs e)
        {
            if (AutosaveIntervalSelector.Items.Contains(AUTOSAVE_DEFAULT_SELECT))
            {
                AutosaveIntervalSelector.Items.RemoveAt(0);
            }
        }

        private void AutosaveIntervalSelector_DropDownClosed(object sender, EventArgs e)
        {
            if(AutosaveIntervalSelector.SelectedItem == null)
            {
                AutosaveIntervalSelector.Items.Insert(0, AUTOSAVE_DEFAULT_SELECT);
                AutosaveIntervalSelector.SelectedIndex = 0;
            };
        }

        private void AutosaveStartBTN_Click(object sender, EventArgs e)
        {
            if (!AutosaveIntervalSelector.SelectedItem.Equals(AUTOSAVE_DEFAULT_SELECT)) {
                AutosaveStopBTN.Enabled = true;
                AutosaveStartBTN.Enabled = false;
                AutoSaveStatusLabel.Text = String.Format(auto_save_label_text, auto_save_status[0]);

                autosave_timer = new System.Timers.Timer();
                autosave_timer.Interval = autosave_interval*60000;
                autosave_timer.Elapsed += OnTimedEvent;
                autosave_timer.Start();
            }
        }

        private void OnTimedEvent(object sender, EventArgs e)
        {
            if (autosave_naming_suffix == 5)
            {
                autosave_naming_suffix = 0;
            }

            if (autosave_count <  autosave_limit)
            {
                autosave_count++;
                autosave_naming_suffix++;
                save_file(SAVE_STYLE.AUTO);
            } else if (autosave_count == 5)
            {
                // delete oldest
                DirectoryInfo autosave_dir = new DirectoryInfo(autosave_location);
                FileInfo autosave_file = autosave_dir.GetFiles().OrderByDescending(file => file.CreationTime).Last();

                File.Delete(autosave_file.FullName);

                autosave_naming_suffix++;
                save_file(SAVE_STYLE.AUTO);
            }
            
        }

        private void AutosaveStopBTN_Click(object sender, EventArgs e)
        {
            AutosaveStartBTN.Enabled = true;
            AutosaveStopBTN.Enabled = false;
            AutoSaveStatusLabel.Text = String.Format(auto_save_label_text, auto_save_status[1]);
            autosave_timer.Stop();
        }

        private void AUTOSVELocationSelectorBTN_Click(object sender, EventArgs e)
        {
            AutoSaveDirDialog = new VistaFolderBrowserDialog();

            AutoSaveDirDialog.ShowDialog();

            string[] save_locations = AutoSaveDirDialog.SelectedPaths;
            if (save_locations != null && save_locations.Length > 0)
            {
                string save_location = save_locations[0];
                AutosaveLocation = save_location;
                AutosaveLocationBox.Text = save_location;

                setNewRestoreLocation(AutosaveLocation);
            }
        }
    }
}