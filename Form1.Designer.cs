﻿namespace EldenRingBackupManager
{
    partial class BackupManagerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// 
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BackupManagerForm));
            this.BackupBTN = new System.Windows.Forms.Button();
            this.RestoreBTN = new System.Windows.Forms.Button();
            this.LocationSelectorBTN = new System.Windows.Forms.Button();
            this.BackupLocationBox = new System.Windows.Forms.TextBox();
            this.BKPLabel = new System.Windows.Forms.Label();
            this.SAVELabel = new System.Windows.Forms.Label();
            this.SaveLocationBox = new System.Windows.Forms.TextBox();
            this.SVELocationSelectorBTN = new System.Windows.Forms.Button();
            this.AutosaveIntervalSelector = new System.Windows.Forms.ComboBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.AutoSaveStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.AutosaveStartBTN = new System.Windows.Forms.Button();
            this.AutosaveStopBTN = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.AutosaveLocationBox = new System.Windows.Forms.TextBox();
            this.AUTOSVELocationSelectorBTN = new System.Windows.Forms.Button();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BackupBTN
            // 
            this.BackupBTN.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BackupBTN.Location = new System.Drawing.Point(301, 41);
            this.BackupBTN.Name = "BackupBTN";
            this.BackupBTN.Size = new System.Drawing.Size(260, 65);
            this.BackupBTN.TabIndex = 0;
            this.BackupBTN.Text = "Quicksave";
            this.BackupBTN.UseVisualStyleBackColor = true;
            this.BackupBTN.Click += new System.EventHandler(this.BackupBTN_Click);
            // 
            // RestoreBTN
            // 
            this.RestoreBTN.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.RestoreBTN.Location = new System.Drawing.Point(301, 112);
            this.RestoreBTN.Name = "RestoreBTN";
            this.RestoreBTN.Size = new System.Drawing.Size(260, 65);
            this.RestoreBTN.TabIndex = 1;
            this.RestoreBTN.Text = "Quick Reload";
            this.RestoreBTN.UseVisualStyleBackColor = true;
            this.RestoreBTN.Click += new System.EventHandler(this.RestoreBTN_Click);
            // 
            // LocationSelectorBTN
            // 
            this.LocationSelectorBTN.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LocationSelectorBTN.Location = new System.Drawing.Point(255, 9);
            this.LocationSelectorBTN.Name = "LocationSelectorBTN";
            this.LocationSelectorBTN.Size = new System.Drawing.Size(23, 23);
            this.LocationSelectorBTN.TabIndex = 3;
            this.LocationSelectorBTN.Text = "...";
            this.LocationSelectorBTN.UseMnemonic = false;
            this.LocationSelectorBTN.UseVisualStyleBackColor = true;
            this.LocationSelectorBTN.Click += new System.EventHandler(this.LocationSelectorBTN_Click);
            // 
            // BackupLocationBox
            // 
            this.BackupLocationBox.Location = new System.Drawing.Point(109, 11);
            this.BackupLocationBox.Name = "BackupLocationBox";
            this.BackupLocationBox.ReadOnly = true;
            this.BackupLocationBox.Size = new System.Drawing.Size(140, 20);
            this.BackupLocationBox.TabIndex = 4;
            // 
            // BKPLabel
            // 
            this.BKPLabel.AutoSize = true;
            this.BKPLabel.Location = new System.Drawing.Point(9, 14);
            this.BKPLabel.Name = "BKPLabel";
            this.BKPLabel.Size = new System.Drawing.Size(88, 13);
            this.BKPLabel.TabIndex = 5;
            this.BKPLabel.Text = "Backup Location";
            // 
            // SAVELabel
            // 
            this.SAVELabel.AutoSize = true;
            this.SAVELabel.Location = new System.Drawing.Point(310, 14);
            this.SAVELabel.Name = "SAVELabel";
            this.SAVELabel.Size = new System.Drawing.Size(76, 13);
            this.SAVELabel.TabIndex = 8;
            this.SAVELabel.Text = "Save Location";
            // 
            // SaveLocationBox
            // 
            this.SaveLocationBox.Location = new System.Drawing.Point(392, 11);
            this.SaveLocationBox.Name = "SaveLocationBox";
            this.SaveLocationBox.ReadOnly = true;
            this.SaveLocationBox.Size = new System.Drawing.Size(140, 20);
            this.SaveLocationBox.TabIndex = 7;
            // 
            // SVELocationSelectorBTN
            // 
            this.SVELocationSelectorBTN.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.SVELocationSelectorBTN.Location = new System.Drawing.Point(538, 9);
            this.SVELocationSelectorBTN.Name = "SVELocationSelectorBTN";
            this.SVELocationSelectorBTN.Size = new System.Drawing.Size(23, 23);
            this.SVELocationSelectorBTN.TabIndex = 6;
            this.SVELocationSelectorBTN.Text = "...";
            this.SVELocationSelectorBTN.UseMnemonic = false;
            this.SVELocationSelectorBTN.UseVisualStyleBackColor = true;
            this.SVELocationSelectorBTN.Click += new System.EventHandler(this.SVELocationSelectorBTN_Click);
            // 
            // AutosaveIntervalSelector
            // 
            this.AutosaveIntervalSelector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AutosaveIntervalSelector.FormattingEnabled = true;
            this.AutosaveIntervalSelector.Items.AddRange(new object[] {
            "5",
            "10",
            "15",
            "30",
            "45",
            "60"});
            this.AutosaveIntervalSelector.Location = new System.Drawing.Point(18, 69);
            this.AutosaveIntervalSelector.Name = "AutosaveIntervalSelector";
            this.AutosaveIntervalSelector.Size = new System.Drawing.Size(260, 21);
            this.AutosaveIntervalSelector.TabIndex = 9;
            this.AutosaveIntervalSelector.DropDown += new System.EventHandler(this.AutosaveIntervalSelector_DropDown);
            this.AutosaveIntervalSelector.SelectedIndexChanged += new System.EventHandler(this.AutosaveIntervalSelector_SelectedIndexChanged);
            this.AutosaveIntervalSelector.DropDownClosed += new System.EventHandler(this.AutosaveIntervalSelector_DropDownClosed);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AutoSaveStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 190);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(573, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 10;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // AutoSaveStatusLabel
            // 
            this.AutoSaveStatusLabel.Name = "AutoSaveStatusLabel";
            this.AutoSaveStatusLabel.Size = new System.Drawing.Size(118, 17);
            this.AutoSaveStatusLabel.Text = "toolStripStatusLabel1";
            // 
            // AutosaveStartBTN
            // 
            this.AutosaveStartBTN.ForeColor = System.Drawing.SystemColors.ControlText;
            this.AutosaveStartBTN.Image = ((System.Drawing.Image)(resources.GetObject("AutosaveStartBTN.Image")));
            this.AutosaveStartBTN.Location = new System.Drawing.Point(121, 96);
            this.AutosaveStartBTN.Name = "AutosaveStartBTN";
            this.AutosaveStartBTN.Size = new System.Drawing.Size(75, 25);
            this.AutosaveStartBTN.TabIndex = 11;
            this.AutosaveStartBTN.Text = "Start";
            this.AutosaveStartBTN.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.AutosaveStartBTN.UseVisualStyleBackColor = true;
            this.AutosaveStartBTN.Click += new System.EventHandler(this.AutosaveStartBTN_Click);
            // 
            // AutosaveStopBTN
            // 
            this.AutosaveStopBTN.Enabled = false;
            this.AutosaveStopBTN.Image = ((System.Drawing.Image)(resources.GetObject("AutosaveStopBTN.Image")));
            this.AutosaveStopBTN.Location = new System.Drawing.Point(203, 96);
            this.AutosaveStopBTN.Name = "AutosaveStopBTN";
            this.AutosaveStopBTN.Size = new System.Drawing.Size(75, 25);
            this.AutosaveStopBTN.TabIndex = 12;
            this.AutosaveStopBTN.Text = "Stop";
            this.AutosaveStopBTN.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.AutosaveStopBTN.UseVisualStyleBackColor = true;
            this.AutosaveStopBTN.Click += new System.EventHandler(this.AutosaveStopBTN_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Autosave Location";
            // 
            // AutosaveLocationBox
            // 
            this.AutosaveLocationBox.Location = new System.Drawing.Point(109, 43);
            this.AutosaveLocationBox.Name = "AutosaveLocationBox";
            this.AutosaveLocationBox.ReadOnly = true;
            this.AutosaveLocationBox.Size = new System.Drawing.Size(140, 20);
            this.AutosaveLocationBox.TabIndex = 14;
            // 
            // AUTOSVELocationSelectorBTN
            // 
            this.AUTOSVELocationSelectorBTN.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.AUTOSVELocationSelectorBTN.Location = new System.Drawing.Point(255, 41);
            this.AUTOSVELocationSelectorBTN.Name = "AUTOSVELocationSelectorBTN";
            this.AUTOSVELocationSelectorBTN.Size = new System.Drawing.Size(23, 23);
            this.AUTOSVELocationSelectorBTN.TabIndex = 13;
            this.AUTOSVELocationSelectorBTN.Text = "...";
            this.AUTOSVELocationSelectorBTN.UseMnemonic = false;
            this.AUTOSVELocationSelectorBTN.UseVisualStyleBackColor = true;
            this.AUTOSVELocationSelectorBTN.Click += new System.EventHandler(this.AUTOSVELocationSelectorBTN_Click);
            // 
            // BackupManagerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 212);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AutosaveLocationBox);
            this.Controls.Add(this.AUTOSVELocationSelectorBTN);
            this.Controls.Add(this.AutosaveStopBTN);
            this.Controls.Add(this.AutosaveStartBTN);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.AutosaveIntervalSelector);
            this.Controls.Add(this.SAVELabel);
            this.Controls.Add(this.SaveLocationBox);
            this.Controls.Add(this.SVELocationSelectorBTN);
            this.Controls.Add(this.BKPLabel);
            this.Controls.Add(this.BackupLocationBox);
            this.Controls.Add(this.LocationSelectorBTN);
            this.Controls.Add(this.RestoreBTN);
            this.Controls.Add(this.BackupBTN);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MinimumSize = new System.Drawing.Size(300, 206);
            this.Name = "BackupManagerForm";
            this.Text = "Backup Manager";
            this.Load += new System.EventHandler(this.BackupManagerForm_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BackupBTN;
        private System.Windows.Forms.Button RestoreBTN;
        private System.Windows.Forms.Button LocationSelectorBTN;
        private System.Windows.Forms.TextBox BackupLocationBox;
        private System.Windows.Forms.Label BKPLabel;
        private System.Windows.Forms.Label SAVELabel;
        private System.Windows.Forms.TextBox SaveLocationBox;
        private System.Windows.Forms.Button SVELocationSelectorBTN;
        private System.Windows.Forms.ComboBox AutosaveIntervalSelector;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel AutoSaveStatusLabel;
        private System.Windows.Forms.Button AutosaveStartBTN;
        private System.Windows.Forms.Button AutosaveStopBTN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox AutosaveLocationBox;
        private System.Windows.Forms.Button AUTOSVELocationSelectorBTN;
    }
}

